ssh Cookbook
============

Installs and configures openssh client and server on Ubuntu.

Attributes
----------

* default[:ssh][:port] = 22
* default[:ssh][:permitroot] = 'no'
* default[:ssh][:x11forwarding] = 'no'

