name             'ssh'
version          '0.1.0'
description      'Installs/Configures ssh client and server'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
license          'Apache 2.0'
maintainer       'Marty Pauley'
maintainer_email 'marty+chef@martian.org'
supports         'ubuntu'
